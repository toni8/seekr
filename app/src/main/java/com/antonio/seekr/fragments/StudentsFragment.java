package com.antonio.seekr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.seekr.adapters.StudentListAdapter;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.antonio.seekr.utils.Utils.hideProgressBar;
import static com.antonio.seekr.utils.Utils.showProgressBar;

/**
 * Created by Antonio on 31-Oct-16.
 */

public class StudentsFragment extends Fragment {
    // Store instance variables
    private int page;
    private ProgressBar spinner;

    RecyclerView recyclerView;
    StudentListAdapter studentListAdapter;
    List<Student> studentList = new ArrayList<>();
    List<Student> filteredStudentList = new ArrayList<>();
    TextView filterTv;

    // newInstance constructor for creating fragment with arguments
    public static StudentsFragment newInstance(int page) {
        StudentsFragment fragmentFirst = new StudentsFragment();
        Bundle args = new Bundle();
        args.putInt("arg_page", page);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("arg_page", 0);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_students, container, false);
        spinner = (ProgressBar) view.findViewById(R.id.progress_bar);
        showProgressBar(spinner);

        filterTv = (TextView) view.findViewById(R.id.filter_text);

        setHasOptionsMenu(true);

        loadStudents();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        studentListAdapter = new StudentListAdapter(getActivity(), filteredStudentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        return view;
    }

    public void loadStudents() {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final Student me = Data.getInstance().getMe();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                studentList.clear();
                hideProgressBar(spinner);
                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    if (!user.getUid().equals(data.getKey())) {
                        //this user is not me

                        Student s = new Student();
                        s.setStudentId(data.getKey());
                        if (data.child("fullName").getValue() != null)
                            s.setFullName(data.child("fullName").getValue().toString());
                        if (data.child("homeCity").getValue() != null)
                            s.setHomeCity(data.child("homeCity").getValue().toString());
                        if (data.child("homeCountry").getValue() != null)
                            s.setHomeCountry(Utils.clearFyrom(data.child("homeCountry").getValue().toString()));
                        if (data.child("hostCity").getValue() != null)
                            s.setHostCity(data.child("hostCity").getValue().toString());
                        if (data.child("hostCountry").getValue() != null)
                            s.setHostCountry(Utils.clearFyrom(data.child("hostCountry").getValue().toString()));
                        if (data.child("emailPrivate").getValue() != null)
                            s.setEmailPrivate(((boolean) data.child("emailPrivate").getValue()));
                        if (data.child("email").getValue() != null)
                            s.setEmail(data.child("email").getValue().toString());

                        studentList.add(s);

                    }
                }

                Data.getInstance().setStudentList(studentList);
                filteredStudentList.addAll(studentList);
                filterByCountry();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    Log.w("FIREBASE", "Failed to read value.", error.toException());
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressBar(spinner);
                }
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter_students, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_all:
                removeFilter();
                return true;

            case R.id.action_city:
                filterByCity();
                return true;

            case R.id.action_country:
                filterByCountry();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void removeFilter() {
        filterTv.setText(getResources().getString(R.string.all_students));
        filteredStudentList.clear();
        filteredStudentList.addAll(studentList);
        studentListAdapter.notifyDataSetChanged();
    }

    private void filterByCountry() {
        filterTv.setText(getResources().getString(R.string.in_your_country));
        filteredStudentList.clear();
        for (Student s : studentList) {
            if (s.getHostCountry() != null)
                if (s.getHostCountry().equalsIgnoreCase(Data.getInstance().getMe().getHostCountry())) {
                    filteredStudentList.add(s);
                }
        }
        studentListAdapter.notifyDataSetChanged();
    }

    private void filterByCity() {
        filterTv.setText(getResources().getString(R.string.in_your_city));
        filteredStudentList.clear();
        for (Student s : studentList) {
            if (s.getHostCity() != null)
                if (s.getHostCity().equalsIgnoreCase(Data.getInstance().getMe().getHostCity())) {
                    filteredStudentList.add(s);
                }
        }
        studentListAdapter.notifyDataSetChanged();
    }

}
