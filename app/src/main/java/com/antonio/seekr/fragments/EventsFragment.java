package com.antonio.seekr.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.seekr.adapters.EventListAdapter;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Event;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.antonio.seekr.utils.Utils.convertStringToDate;
import static com.antonio.seekr.utils.Utils.hideProgressBar;
import static com.antonio.seekr.utils.Utils.reformatStringDate;
import static com.antonio.seekr.utils.Utils.showProgressBar;

/**
 * Created by Antonio on 31-Oct-16.
 */

public class EventsFragment extends Fragment {
    // Store instance variables
    private int page;
    private ProgressBar spinner;

    RecyclerView recyclerView;
    EventListAdapter eventListAdapter;
    List<Event> eventList = new ArrayList<>();
    List<Event> filteredEventList = new ArrayList<>();
    TextView filterTv;

    // newInstance constructor for creating fragment with arguments
    public static EventsFragment newInstance(int page) {
        EventsFragment fragmentFirst = new EventsFragment();
        Bundle args = new Bundle();
        args.putInt("arg_page", page);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("arg_page", 0);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_events, container, false);

        setHasOptionsMenu(true);

        filterTv = (TextView) view.findViewById(R.id.filter_text);

        spinner = (ProgressBar) view.findViewById(R.id.progress_bar);
        showProgressBar(spinner);

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("events");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                eventList.clear();
                hideProgressBar(spinner);
                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    Event e = new Event();
                    e.setId(data.getKey());
                    if (data.child("title").getValue() != null)
                        e.setTitle(data.child("title").getValue().toString());
                    if (data.child("description").getValue() != null)
                        e.setDescription(data.child("description").getValue().toString());
                    if (data.child("creator").getValue() != null)
                        e.setCreator(data.child("creator").getValue().toString());
                    if (data.child("creatorId").getValue() != null)
                        e.setCreatorId(data.child("creatorId").getValue().toString());
                    if (data.child("time").getValue() != null) {
                        e.setTime(reformatStringDate(data.child("time").getValue().toString()));
                    }
                    if (data.child("country").getValue() != null) {
                        e.setCountry(Utils.clearFyrom(data.child("country").getValue().toString()));
                    }
                    if (data.child("city").getValue() != null)
                        e.setCity(data.child("city").getValue().toString());
                    if (data.child("color").getValue() != null)
                        e.setColor(data.child("color").getValue().toString());
                    eventList.add(e);

                }

                Collections.sort(eventList, new Comparator<Event>() {
                    public int compare(Event e1, Event e2) {
                        return convertStringToDate(e1.getTime()).compareTo(convertStringToDate(e2.getTime()));
                    }
                });

                Data.getInstance().setEventList(eventList);
                filteredEventList.addAll(eventList);
                filterByCountry();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    Log.w("FIREBASE", "Failed to read value.", error.toException());
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressBar(spinner);
                }
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        eventListAdapter = new EventListAdapter(getActivity(), filteredEventList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventListAdapter);


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter_events, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_all:
                removeFilter();
                return true;

            case R.id.action_city:
                filterByCity();
                return true;

            case R.id.action_country:
                filterByCountry();
                return true;

            default:

                return super.onOptionsItemSelected(item);

        }
    }


    private void removeFilter() {
        filterTv.setText(getResources().getString(R.string.all_events));
        filteredEventList.clear();
        filteredEventList.addAll(eventList);
        eventListAdapter.notifyDataSetChanged();
    }

    private void filterByCountry() {
        filterTv.setText(getResources().getString(R.string.in_your_country));
        filteredEventList.clear();
        for (Event e : eventList) {
            if (e.getCountry().equalsIgnoreCase(Data.getInstance().getMe().getHostCountry())) {
                filteredEventList.add(e);
            }
        }
        eventListAdapter.notifyDataSetChanged();
    }

    private void filterByCity() {
        filterTv.setText(getResources().getString(R.string.in_your_city));
        filteredEventList.clear();
        for (Event e : eventList) {
            if (e.getCity().equalsIgnoreCase(Data.getInstance().getMe().getHostCity())) {
                filteredEventList.add(e);
            }
        }
        eventListAdapter.notifyDataSetChanged();
    }


}
