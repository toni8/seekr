package com.antonio.seekr.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.antonio.seekr.R;
import com.antonio.seekr.adapters.EventListAdapter;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Event;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;
import static com.antonio.seekr.utils.Constants.STORAGE_PROFILE_PHOTOS;
import static com.antonio.seekr.utils.Constants.URL_STORAGE;

/**
 * Created by Antonio on 04/11/2016.
 */

public class Utils {

    /**
     * Sets the margins to the specified view
     *
     * @param v The View to set the margins to.
     * @param l Left margin
     * @param t Top margin
     * @param r Right margin
     * @param b Bottom margin
     */

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }


    /**
     * Changes the style of the page number View to indicate that it has been selected
     *
     * @param firstView  View that will focused
     * @param secondView View to be defocused
     * @param thirdView  View to be defocused
     */

    public static void selectPage(View firstView, View secondView, View thirdView) {
        if (firstView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams
                && secondView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams
                && thirdView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p1 = (ViewGroup.MarginLayoutParams) firstView.getLayoutParams();
            ViewGroup.MarginLayoutParams p2 = (ViewGroup.MarginLayoutParams) secondView.getLayoutParams();
            ViewGroup.MarginLayoutParams p3 = (ViewGroup.MarginLayoutParams) thirdView.getLayoutParams();

            p1.setMargins(0, 0, 0, 24);
            p2.setMargins(0, 0, 0, 0);
            p3.setMargins(0, 0, 0, 0);

            //view that has been selected
            firstView.setAlpha(1.0f);
            firstView.requestLayout();

            //unselected views
            secondView.setAlpha(0.5f);
            thirdView.setAlpha(0.5f);
            secondView.requestLayout();
            thirdView.requestLayout();

        }
    }

    /**
     * Plays Seekr notification sound
     *
     * @param ctx The context
     */
    public static void playSound(Context ctx) {
        MediaPlayer m = new MediaPlayer();
        try {
            AssetFileDescriptor descriptor = ctx.getAssets().openFd("success_registration.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            m.prepare();
            m.start();
        } catch (Exception e) {
            // handle error here..
        }
    }

    /**
     * Full screen mode
     *
     * @param act The activity
     */
    public static void fullScreen(Activity act) {

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = act.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.i(TAG, "Turning immersive mode mode off. ");
        } else {
            Log.i(TAG, "Turning immersive mode mode on.");
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        act.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }


    /**
     * Upload users profile photo
     *
     * @param activity Activity
     * @param uri      The photo uri
     * @param user     Current user
     */

    public static void uploadProfilePhoto(final Activity activity, Uri uri, FirebaseUser user) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(URL_STORAGE)
                .child(STORAGE_PROFILE_PHOTOS)
                .child(user.getUid() + ".jpg");

        if (uri != null) {
            UploadTask uploadTask = storageRef.putFile(uri);


            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    System.out.println("Upload is " + progress + "% done");
                }
            }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                    System.out.println("Upload is paused");
                }
            });


            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(activity, exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                }
            });

        }
    }

    /**
     * Sets users profile photos
     *
     * @param activity Activity
     * @param iv       Image view
     * @param userId   User's userId
     */
    public static void setProfilePhoto(final Activity activity, final ImageView iv, String userId) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(URL_STORAGE)
                .child(STORAGE_PROFILE_PHOTOS)
                .child(userId + ".jpg");

        Glide.with(activity /* context */)
                .using(new FirebaseImageLoader())
                .load(storageRef)
                .asBitmap()
                .into(new BitmapImageViewTarget(iv) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        iv.setImageDrawable(circularBitmapDrawable);
                    }
                });

    }

    /**
     * Clear FYROM texts
     *
     * @param country Country text
     */

    public static String clearFyrom(String country) {

        String countryClear = country;
        if (country.contains("(FYROM)")) {
            countryClear = country.replace("(FYROM)", "");
        }

        return countryClear.trim();
    }

    /**
     * Hides the progress spinner
     *
     * @param pb ProgressBar view
     */
    public static void hideProgressBar(ProgressBar pb) {
        pb.setVisibility(View.GONE);
    }

    /**
     * Shows the progress spinner
     *
     * @param pb ProgressBar view
     */
    public static void showProgressBar(ProgressBar pb) {
        pb.setVisibility(View.VISIBLE);
    }


    /**
     * Removes event from the list
     *
     * @param position Position of the event to be removed
     * @param list     List of all events
     * @param adapter  Event List adapter
     */

    public static void removeEvent(int position, List<Event> list, EventListAdapter adapter) {
        list.remove(position);
        Data.getInstance().setEventList(list);
        adapter.notifyItemRemoved(position);
        adapter.notifyDataSetChanged();

    }

    /**
     * Adds event to the list
     *
     * @param event    The event to be added
     * @param position Position of the new event
     * @param list     List of all events
     * @param adapter  Event List adapter
     */
    public static void addEvent(Event event, int position, List<Event> list, EventListAdapter adapter) {
        list.add(position, event);
        Data.getInstance().setEventList(list);
        adapter.notifyItemInserted(position);
        adapter.notifyDataSetChanged();
    }

    /**
     * Reformats given string date
     *
     * @param string The string to be reformatted
     * @return Final reformatted string
     */
    public static String reformatStringDate(String string) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        Date myDate = null;
        try {
            myDate = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        String finalDate = timeFormat.format(myDate);

        return finalDate;
    }

    /**
     * Converts String to Date
     *
     * @param string The string to be converted
     * @return Date
     */
    public static Date convertStringToDate(String string) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);
        Date finaldate = null;
        try {
            finaldate = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finaldate;
    }

    /**
     * Displays normal info dialog with title and message and CLOSE button
     *
     * @param act     Calling activity
     * @param title   Dialog Title
     * @param message Dialog Message
     */
    public static void showInfoDialog(Activity act, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(act, R.style.Theme_AppCompat_Light_Dialog_Alert).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CLOSE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
