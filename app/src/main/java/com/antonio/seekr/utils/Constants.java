package com.antonio.seekr.utils;

/**
 * Created by Antonio on 04/11/2016.
 */

public class Constants {

    public static final String LOGIN = "L O G  I N";
    public static final String SIGNUP = "S I G N  U P";
    public static final String STORAGE_PROFILE_PHOTOS = "profilephotos";
    public static final String URL_STORAGE = "gs://seekr-7684c.appspot.com";

    public static final String EXTRA_CHAT_USER_ID = "extra_chat_user_id";
}
