package com.antonio.seekr.utils;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * Created by Antonio on 19-Oct-16.
 */

public class Validators {

    private static Activity act;

    public Validators(Activity act) {
        this.act = act;
    }

    public boolean validateEmail(EditText inputEmail, TextInputLayout inputLayoutEmail) {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty()) {
            inputLayoutEmail.setError("Please enter email");
            requestFocus(inputEmail);
            return false;
        } else if (!isValidEmail(email)) {
            inputLayoutEmail.setError("Invalid email");
            requestFocus(inputEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validatePassword(EditText inputPassword, TextInputLayout inputLayoutPassword, boolean isRegistering) {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError("Please enter password");
            requestFocus(inputPassword);
            return false;
        } else if (inputPassword.getText().toString().trim().length() < 6 && isRegistering) {
            inputLayoutPassword.setError("Password needs to be at least 6 characters long");
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateConfirmPassword(EditText inputPassword, EditText inputConfirmPassword, TextInputLayout inputLayoutConfirmPassword) {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutConfirmPassword.setError("Please confirm your password");
            requestFocus(inputConfirmPassword);
            return false;
        } else if (!inputPassword.getText().toString().trim().equals(inputConfirmPassword.getText().toString().trim())) {
            inputLayoutConfirmPassword.setError("Passwords need to match");
            requestFocus(inputConfirmPassword);
            return false;
        } else {
            inputLayoutConfirmPassword.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateRequiredField(EditText editText, TextInputLayout inputLayout) {
        if (editText.getText().toString().trim().isEmpty()) {
            inputLayout.setError("This field is required.");
            requestFocus(editText);
            return false;
        } else {
            inputLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void requestFocus(View view) {
        if (view.requestFocus()) {
            if (act != null)
                act.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}
