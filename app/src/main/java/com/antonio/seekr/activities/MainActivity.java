package com.antonio.seekr.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antonio.seekr.R;
import com.antonio.seekr.adapters.FragmentsAdapter;
import com.antonio.seekr.adapters.StudentListAdapter;
import com.antonio.seekr.models.Event;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.utils.SharedPrefsUtil;
import com.antonio.seekr.utils.Utils;
import com.gigamole.navigationtabbar.ntb.NavigationTabBar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.AddressComponentType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class MainActivity extends BaseActivity {

    RecyclerView recyclerView;
    StudentListAdapter studentListAdapter;
    List<Student> studentList = new ArrayList<>();
    Toolbar toolbar;
    FragmentsAdapter adapterViewPager;
    FloatingActionButton fab;
    private DatabaseReference mDatabase;

    TextView tvDate;
    TextView tvTime;
    EditText etTitle, etDescription;
    PlacesAutocompleteTextView atvLocation;
    Place chosenLocation;
    String country, city;
    String chosenDate, chosenTime;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_coordinator_ntb;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        viewPager.setOffscreenPageLimit(2); //do not recreate fragments that are close to each other

        adapterViewPager = new FragmentsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        final String[] colors = getResources().getStringArray(R.array.default_preview);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        ContextCompat.getDrawable(this, R.drawable.ic_event_white_24dp),
                        Color.parseColor(colors[0]))
                        .title("Events")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        ContextCompat.getDrawable(this, R.drawable.ic_people_white_24dp),
                        Color.parseColor(colors[1]))
                        .title("Students")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        ContextCompat.getDrawable(this, R.drawable.ic_menu_white_24dp),
                        Color.parseColor(colors[2]))
                        .title("Settings")
                        .build()
        );

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 0);

        //IMPORTANT: ENABLE SCROLL BEHAVIOUR IN COORDINATOR LAYOUT
        navigationTabBar.setBehaviorEnabled(true);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {
                if (index == 1 || index == 2)
                    fab.hide();
                else
                    fab.show();
            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
                navigationTabBar.show();
            }
        });
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (!navigationTabBar.isShown())
                    navigationTabBar.show();
            }

            @Override
            public void onPageSelected(final int position) {
                if (position == 1 || position == 2)
                    fab.hide();
                else
                    fab.show();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });


        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("NEW EVENT")
                .titleColor(Color.WHITE)
                .customView(R.layout.dialog_new_event, true)
                .positiveText("Next")
                .titleColor(ContextCompat.getColor(this, R.color.colorTextPrimary))
                .neutralText("Cancel")
                .titleGravity(GravityEnum.CENTER)
                .build();

        tvDate = (TextView) dialog.getCustomView().findViewById(R.id.tv_date);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        tvTime = (TextView) dialog.getCustomView().findViewById(R.id.tv_time);
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker();
            }
        });

        etTitle = (EditText) dialog.getCustomView().findViewById(R.id.et_event_title);
        etDescription = (EditText) dialog.getCustomView().findViewById(R.id.et_event_desc);
        atvLocation = (PlacesAutocompleteTextView) dialog.getCustomView().findViewById(R.id.places_autocomplete);

        atvLocation.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {
                        chosenLocation = place;

                        atvLocation.getDetailsFor(place, new DetailsCallback() {
                            @Override
                            public void onSuccess(final PlaceDetails details) {
                                for (AddressComponent component : details.address_components) {
                                    for (AddressComponentType type : component.types) {
                                        city = details.vicinity;
                                        switch (type) {
                                            case STREET_NUMBER:
                                                break;
                                            case ROUTE:
                                                break;
                                            case NEIGHBORHOOD:
                                                break;
                                            case SUBLOCALITY_LEVEL_1:
                                                break;
                                            case SUBLOCALITY:
                                                break;
                                            case LOCALITY:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_1:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_2:
                                                break;
                                            case COUNTRY:
                                                country = Utils.clearFyrom(component.long_name);
                                                break;
                                            case POSTAL_CODE:
                                                break;
                                            case POLITICAL:
                                                break;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final Throwable failure) {
                                Log.d("test", "failure " + failure);
                            }
                        });

                    }
                }
        );

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.parent);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dialog.show();
            }
/*                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final String title = String.valueOf(new Random().nextInt(15));
                            if (!model.isBadgeShowed()) {
                                model.setBadgeTitle(title);
                                model.showBadge();
                            } else model.updateBadgeTitle(title);
                        }
                    }, i * 100);

                coordinatorLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Snackbar snackbar = Snackbar.make(navigationTabBar, "Coordinator NTB", Snackbar.LENGTH_SHORT);
                        snackbar.getView().setBackgroundColor(Color.parseColor("#9b92b3"));
                        ((TextView) snackbar.getView().findViewById(R.id.snackbar_text))
                                .setTextColor(Color.parseColor("#423752"));
                        snackbar.show();
                    }
                }, 1000);*/

        });

        dialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chosenLocation == null) {
                    Toast.makeText(MainActivity.this, "Please choose a location", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.hide();
                showRadioButtonDialog();
            }
        });


        if (SharedPrefsUtil.getBooleanPreference(this, "show_new_event_prompt", true)) {
            new MaterialTapTargetPrompt.Builder(MainActivity.this)
                    .setTarget(findViewById(R.id.fab))
                    .setPrimaryText("Add your first event")
                    .setSecondaryText("Tap the plus button to start creating new event.")
                    .setBackgroundColour(ContextCompat.getColor(this, R.color.light_green))
                    .setOnHidePromptListener(new MaterialTapTargetPrompt.OnHidePromptListener() {
                        @Override
                        public void onHidePrompt(MotionEvent event, boolean tappedTarget) {
                            //Do something such as storing a value so that this prompt is never shown again
                            SharedPrefsUtil.setBooleanPreference(MainActivity.this, "show_new_event_prompt", false);
                        }

                        @Override
                        public void onHidePromptComplete() {

                        }
                    })
                    .show();
        }
    }

    private void showRadioButtonDialog() {

        final String[] colors = getResources().getStringArray(R.array.colors);


        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (8 * scale + 0.5f);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(24, 24, 24, 24);


        final MaterialDialog colorDialog = new MaterialDialog.Builder(this)
                .title("PICK COLOR")
                .titleColor(Color.WHITE)
                .customView(R.layout.dialog_radio_button, true)
                .positiveText("Post")
                .neutralText("Cancel")
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .titleColor(ContextCompat.getColor(this, R.color.colorTextPrimary))
                .titleGravity(GravityEnum.CENTER)
                .build();

        final RadioGroup rg = (RadioGroup) colorDialog.findViewById(R.id.radio_group);

        for (int i = 0; i < colors.length; i++) {
            RadioButton rb = new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setBackgroundColor(Color.parseColor(colors[i]));
            rb.setText(colors[i]);
            rb.setTextColor(Color.WHITE);
            rb.setLayoutParams(params);
            rb.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
            rg.addView(rb);
        }

        colorDialog.show();

        colorDialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                Event event = new Event();

                for (int i = 0; i < rg.getChildCount(); i++) {
                    RadioButton currRadioBtn = (RadioButton) rg.getChildAt(i);
                    if (currRadioBtn.isChecked()) {
                        event.setColor(currRadioBtn.getText().toString());
                    }
                }

                event.setTitle(etTitle.getText().toString());
                event.setDescription(etDescription.getText().toString());
                event.setCountry(country);
                event.setCity(city);
                event.setTime(chosenDate + " " + chosenTime);
                event.setCreatorId(user.getUid());
                event.setCreator(user.getDisplayName());
                mDatabase.child("events").push().setValue(event);

                colorDialog.hide();
            }
        });

    }

    private void showDatePicker() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        tvDate.setText("Date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        chosenDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTimePicker() {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        tvTime.setText("Time: " + hourOfDay + ":" + minute);
                        chosenTime = hourOfDay + ":" + minute;
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }
}



