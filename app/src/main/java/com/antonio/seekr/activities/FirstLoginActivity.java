package com.antonio.seekr.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.antonio.seekr.adapters.FirstLoginPagerAdapter;
import com.antonio.seekr.custom_views.CustomViewPager;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.extra.Scale;

import java.io.File;
import java.io.IOException;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class FirstLoginActivity extends AppCompatActivity {

    ImageButton pageOne, pageTwo, pageThree;
    ViewGroup transitionsContainer, successRegistrationContainer;
    ImageButton btnSubmit;
    ImageView ivSuccess;
    ImageView ivProfilePhoto;
    CustomViewPager viewPager;
    private Uri finalUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_login);


        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new FirstLoginPagerAdapter(this));
        viewPager.setPagingEnabled(false);

        transitionsContainer = (ViewGroup) findViewById(R.id.page_buttons_container);
        successRegistrationContainer = (ViewGroup) findViewById(R.id.success_register_container);
        ivSuccess = (ImageView) successRegistrationContainer.findViewById(R.id.iv_register_success);

        pageOne = (ImageButton) findViewById(R.id.page_one);
        pageTwo = (ImageButton) findViewById(R.id.page_two);
        pageThree = (ImageButton) findViewById(R.id.page_three);

        //select first page
        Utils.selectPage(pageOne, pageTwo, pageThree);

        pageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransitionManager.beginDelayedTransition(transitionsContainer);
                Utils.selectPage(pageOne, pageTwo, pageThree);
                goToPage(0);
            }
        });

        pageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransitionManager.beginDelayedTransition(transitionsContainer);
                Utils.selectPage(pageTwo, pageOne, pageThree);
                goToPage(1);
            }
        });

        pageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransitionManager.beginDelayedTransition(transitionsContainer);
                Utils.selectPage(pageThree, pageOne, pageTwo);
                goToPage(2);
            }
        });
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpTo(this, new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();  // optional depending on your needs
    }

    public void updateUserInfo(final String fullName, final String homeCity, final String hostCity, final String homeCountry, final String hostCountry, final String homeUni, final String hostUni) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Utils.uploadProfilePhoto(this, finalUri, user);

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(fullName) // full name text
                .setPhotoUri(finalUri) //cropped img uri
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //to update student info
                            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                            Student student = new Student();
                            student.setFullName(fullName);
                            student.setFirstLogin(0);

                            student.setHomeCountry(homeCountry);
                            student.setHostCountry(hostCountry);

                            student.setHomeCity(homeCity);
                            student.setHostCity(hostCity);

                            student.setHomeUni(homeUni);
                            student.setHostUni(hostUni);

                            mDatabase.child("users").child(user.getUid()).setValue(student);

                            Data.getInstance().setMe(student);

                            successRegistration();
                        }
                    }
                });

    }

    public void goToPage(int i) {
        viewPager.setCurrentItem(i, true);
        if (i == 0)
            Utils.selectPage(pageOne, pageTwo, pageThree);
        else if (i == 1)
            Utils.selectPage(pageTwo, pageOne, pageThree);
    }

    public void successRegistration() {

        successRegistrationContainer.setVisibility(View.VISIBLE);
        Transition scaleTransition = new Scale();
        scaleTransition.setDuration(500);
        TransitionManager.beginDelayedTransition(successRegistrationContainer, scaleTransition);
        ivSuccess.setVisibility(View.VISIBLE);

        Utils.playSound(this);
        successRegistrationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivSuccess.setVisibility(View.INVISIBLE);
                successRegistrationContainer.setVisibility(View.INVISIBLE);

                Intent intent = new Intent(FirstLoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    public void photoChooser(ImageView p) {
        ivProfilePhoto = p;
        EasyImage.openChooserWithGallery(this, "Choose photo", 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                finalUri = result.getUri();
             //   civProfilePhoto.setImageURI(finalUri);

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), finalUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                circularBitmapDrawable.setCircular(true);
                ivProfilePhoto.setImageDrawable(circularBitmapDrawable);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {

            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    //Some error handling
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    //Handle the image
                    //onPhotoReturned(imageFile);
            /*    Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                civProfilePhoto.setImageBitmap(myBitmap);*/

                    CropImage.activity(Uri.fromFile(imageFile))
                            .setAspectRatio(1, 1)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(FirstLoginActivity.this);

                }
            });
        }
    }
/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, LoginActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

}
