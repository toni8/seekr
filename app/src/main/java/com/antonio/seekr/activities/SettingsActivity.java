package com.antonio.seekr.activities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.antonio.seekr.R;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Student;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SettingsActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showBackButton();

        final Student s = Data.getInstance().getMe();

        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;

        TextView tvVer = (TextView) findViewById(R.id.tv_version);
        tvVer.setText("Seekr Ver. " + version);

        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Switch emailSwitch = (Switch) findViewById(R.id.email_switch);
        emailSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mDatabase.child("users").child(user.getUid()).child("emailPrivate").setValue(b);
                s.setEmailPrivate(b);
                Data.getInstance().setMe(s);
            }
        });


        if (s != null)
            emailSwitch.setChecked(s.isEmailPrivate());

    }

}
