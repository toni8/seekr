package com.antonio.seekr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.antonio.seekr.R;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.utils.Constants;
import com.antonio.seekr.utils.Utils;
import com.antonio.seekr.utils.Validators;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.extra.Scale;

public class LoginActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private FirebaseAuth firebaseAuth;
    private EditText inputEmail, inputPassword, inputConfirmPassword;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword, inputLayoutConfirmPassword;
    private Button btnChangeLayout, btnSubmit;
    private boolean isRegistering = false;
    private String email, password;
    private String btnText = "";
    private Validators validators;
    private ProgressBar progressBar;
    private int firstLogin = 1;
    ViewGroup transitionsContainer, successRegistrationContainer;
    ImageView ivSuccess;
    TextView tvForgotPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        validators = new Validators(this);

        transitionsContainer = (ViewGroup) findViewById(R.id.activity_login_rl);
        successRegistrationContainer = (ViewGroup) findViewById(R.id.success_register_container);

        tvForgotPass = (TextView) findViewById(R.id.tv_forgot_pass);

        ivSuccess = (ImageView) findViewById(R.id.iv_register_success);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnChangeLayout = (Button) findViewById(R.id.btn_change_layout);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputConfirmPassword = (EditText) findViewById(R.id.input_confirm_password);

        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.input_layout_confirm_password);

        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title("Enter your email")
                        .backgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.white))
                        .itemsColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                        .negativeColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                        .contentColor(ContextCompat.getColor(LoginActivity.this, R.color.colorTextPrimary))
                        .titleColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary))
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .input("Email", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                FirebaseAuth.getInstance().sendPasswordResetEmail(input.toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Utils.showInfoDialog(LoginActivity.this
                                                            , "Email sent."
                                                            , "A password reset email has been sent to your address.");

                                                } else {
                                                    Utils.showInfoDialog(LoginActivity.this
                                                            , "Email not sent."
                                                            , "There has been an error. Please check your address and try again.");
                                                }
                                            }
                                        });
                            }
                        }).show();

            }
        });


        btnChangeLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                changeLayout();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //  finalizeRegistration();
/*                if(isRegistering)
                    finalizeRegistration();
                else {
                    startActivity(new Intent(LoginActivity.this, FirstLoginActivity.class));
                    overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
                }*/

                if (isRegistering) {
                    registerUser();
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(R.id.activity_login_rl).getWindowToken(), 0);
                    loginUser();
                }
            }
        });

    }

    private void registerUser() {


        if (!validators.validateEmail(inputEmail, inputLayoutEmail)) {
            return;
        }

        if (!validators.validatePassword(inputPassword, inputLayoutPassword, isRegistering)) {
            return;
        }

        if (!validators.validateConfirmPassword(inputPassword, inputConfirmPassword, inputLayoutPassword)) {
            return;
        }

        handleButtonProgress();

        email = inputEmail.getText().toString().trim();
        password = inputPassword.getText().toString().trim();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        handleButtonProgress();

                        if (task.isSuccessful()) {
                            final FirebaseUser user = firebaseAuth.getCurrentUser();
                            // task.getResult().getUser();
                            if (user != null) {

                                AuthCredential credential = EmailAuthProvider
                                        .getCredential(email, password);

                                user.reauthenticate(credential)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                user.sendEmailVerification()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    mDatabase = FirebaseDatabase.getInstance().getReference();
                                                                    Student student = new Student();
                                                                    student.setFirstLogin(1);
                                                                    student.setStudentId(user.getUid());
                                                                    mDatabase.child("users").child(user.getUid()).setValue(student);
                                                                    finalizeRegistration();
                                                                } else {
                                                                    Utils.showInfoDialog(LoginActivity.this
                                                                            , "Error"
                                                                            , "Error sending verification email. Please contact us to resolve this problem.");
                                                                }
                                                            }
                                                        });
                                            }
                                        });
                            }
                        } else {
                            Utils.showInfoDialog(LoginActivity.this
                                    , "Error"
                                    , "Registration Error");
                        }

                    }
                });
    }

    private void finalizeRegistration() {

        successRegistrationContainer.setVisibility(View.VISIBLE);
        Transition scaleTransition = new Scale();
        scaleTransition.setDuration(500);
        TransitionManager.beginDelayedTransition(successRegistrationContainer, scaleTransition);
        ivSuccess.setVisibility(View.VISIBLE);

        Utils.playSound(this);

        successRegistrationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLayout();
            }
        });
    }

    private void changeLayout() {
        isRegistering = !isRegistering;
        TransitionManager.beginDelayedTransition(transitionsContainer);
        inputLayoutConfirmPassword.setVisibility(isRegistering ? View.VISIBLE : View.GONE);
        btnChangeLayout.setText(isRegistering ? Constants.LOGIN : Constants.SIGNUP);
        btnSubmit.setText(isRegistering ? Constants.SIGNUP : Constants.LOGIN);

        //if registration has been completed, hide the overlay and clear the edittexts
        if (successRegistrationContainer.getVisibility() == View.VISIBLE) {
            successRegistrationContainer.setVisibility(View.INVISIBLE);
            ivSuccess.setVisibility(View.INVISIBLE);
            inputEmail.setText("");
            inputPassword.setText("");
            inputConfirmPassword.setText("");
        }
    }

    private void loginUser() {

        if (!validators.validateEmail(inputEmail, inputLayoutEmail)) {
            return;
        }

        if (!validators.validatePassword(inputPassword, inputLayoutPassword, isRegistering)) {
            return;
        }

        handleButtonProgress();

        email = inputEmail.getText().toString().trim();
        password = inputPassword.getText().toString().trim();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (task.isSuccessful()) {
                            if (task.getResult().getUser().isEmailVerified()) {
                                firstTimeLogin(task.getResult().getUser().getUid());
                            } else {
                                handleButtonProgress();
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(findViewById(R.id.activity_login_rl).getWindowToken(), 0);
                                Snackbar.make(findViewById(R.id.activity_login_rl), "Please verify your email address.", Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            handleButtonProgress();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(findViewById(R.id.activity_login_rl).getWindowToken(), 0);
                            Utils.showInfoDialog(LoginActivity.this
                                    , "Error"
                                    , "Invalid email and/or password");
                        }
                    }
                });
    }

    private void handleButtonProgress() {

        if (!btnSubmit.getText().toString().equals(""))
            btnText = btnSubmit.getText().toString();

        if (progressBar.getVisibility() == View.VISIBLE) {
            btnSubmit.setText(btnText);
            btnSubmit.setEnabled(true);
            progressBar.setVisibility(View.GONE);
        } else {
            btnSubmit.setText("");
            btnSubmit.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    private void firstTimeLogin(final String id) {
        mDatabase = FirebaseDatabase.getInstance().getReference("users").child(id);

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Student s = dataSnapshot.getValue(Student.class);
                if (s.getFirstLogin() == 1) {
                    startActivity(new Intent(LoginActivity.this, FirstLoginActivity.class));
                    overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
                    finish();
                } else {
                    Data.getInstance().setMe(s);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleButtonProgress();
            }
        };
        mDatabase.addListenerForSingleValueEvent(postListener);
    }
}
