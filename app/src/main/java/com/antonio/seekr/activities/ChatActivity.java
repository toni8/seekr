package com.antonio.seekr.activities;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.seekr.R;
import com.antonio.seekr.adapters.ChatListAdapter;
import com.antonio.seekr.models.Chat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.antonio.seekr.utils.Constants.EXTRA_CHAT_USER_ID;
import static com.antonio.seekr.utils.Utils.hideProgressBar;
import static com.antonio.seekr.utils.Utils.showProgressBar;

public class ChatActivity extends BaseActivity {

    RecyclerView recyclerView;
    ChatListAdapter adapter;
    ArrayList<Chat> chatList;
    FirebaseUser user;
    DatabaseReference database;
    EditText etMessage;
    ProgressBar spinner;
    ImageButton btnSend;
    String chatUserID;
    TextView tvChatTyping;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_chat;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showBackButton();

        spinner = (ProgressBar) findViewById(R.id.progress_bar);
        showProgressBar(spinner);

        chatUserID = getIntent().getExtras().getString(EXTRA_CHAT_USER_ID);

        etMessage = (EditText) findViewById(R.id.et_chat_msg);
        tvChatTyping = (TextView) findViewById(R.id.tv_chat_typing);
        btnSend = (ImageButton) findViewById(R.id.btn_chat_send);
        btnSend.setEnabled(false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference();

        chatList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.chat_recycler_view);
        adapter = new ChatListAdapter(this, user.getUid(), chatList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        database.child("chat").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(chatUserID + "+" + user.getUid())) {
                    database = database.child("chat").child(chatUserID + "+" + user.getUid());
                } else if (dataSnapshot.hasChild(chatUserID + "+" + user.getUid())) {
                    database = database.child("chat").child(user.getUid() + "+" + chatUserID);
                } else {
                    database = database.child("chat").child(user.getUid() + "+" + chatUserID);
                }

                addChatListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressBar(spinner);
                Toast.makeText(ChatActivity.this, "Error retrieving chat", Toast.LENGTH_SHORT).show();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etMessage.getText().toString().isEmpty()) {
                    database.push().setValue(user.getUid() + ":" + etMessage.getText().toString());
                    etMessage.setText("");
                }
            }
        });
    }

    ValueEventListener chatValueListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            chatList.clear();

            for (DataSnapshot data : dataSnapshot.getChildren()) {
                if (!(data.getValue() instanceof Boolean)) {
                    Chat chat = new Chat();
                    String msg = data.getValue().toString();
                    chat.setMessage(msg.substring(msg.indexOf(':') + 1));
                    chat.setID(msg.substring(0, msg.indexOf(':')));
                    chatList.add(chat);
                }
            }

            adapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(chatList.size() - 1);

            hideProgressBar(spinner);
            btnSend.setEnabled(true);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Failed to read value
            hideProgressBar(spinner);
            Toast.makeText(ChatActivity.this, "Error retrieving chat", Toast.LENGTH_SHORT).show();
        }
    };

    ValueEventListener typingValueListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() != null) {
                if ((Boolean) dataSnapshot.getValue()) {
                    tvChatTyping.setVisibility(View.VISIBLE);
                } else {
                    tvChatTyping.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private void addChatListener() {

        database.addValueEventListener(chatValueListener);
        etMessage.addTextChangedListener(textWatcher);
        database.child(chatUserID + "typing").addValueEventListener(typingValueListener);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.removeEventListener(chatValueListener);
        database.removeEventListener(typingValueListener);
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // DO THE CALCULATIONS HERE AND SHOW THE RESULT AS PER YOUR CALCULATIONS
            if (count > 0) {
                database.child(user.getUid() + "typing").setValue(true);
            } else if (count == 0) {
                database.child(user.getUid() + "typing").setValue(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

}
