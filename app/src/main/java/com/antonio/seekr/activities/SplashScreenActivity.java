package com.antonio.seekr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.antonio.seekr.R;
import com.antonio.seekr.data.Data;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            // User is logged in

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference mDatabaseUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
            mDatabaseUser.keepSynced(false);

            mDatabaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Data.getInstance().setMe(dataSnapshot.getValue(Student.class));
                    if(Data.getInstance().getMe().getFirstLogin() == 1) {
                        startActivity(new Intent(SplashScreenActivity.this, FirstLoginActivity.class));
                        overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utils.showInfoDialog(SplashScreenActivity.this
                            , "Error"
                            , databaseError.getMessage());
                }
            });

        } else {
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
