package com.antonio.seekr.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.antonio.seekr.activities.ChatActivity;
import com.antonio.seekr.models.Student;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;

import java.util.List;

import static com.antonio.seekr.utils.Constants.EXTRA_CHAT_USER_ID;

/**
 * Created by Antonio on 25-Oct-16.
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {

    private List<Student> studentList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView student_name, student_home, student_host, student_contact;
        ImageView student_photo;
        RelativeLayout student_container;

        public MyViewHolder(View view) {
            super(view);
            student_name = (TextView) view.findViewById(R.id.student_name);
            student_home = (TextView) view.findViewById(R.id.student_home);
            student_host = (TextView) view.findViewById(R.id.student_host);
            student_contact = (TextView) view.findViewById(R.id.student_contact);
            student_photo = (ImageView) view.findViewById(R.id.student_profile_photo);
            student_container = (RelativeLayout) view.findViewById(R.id.rl_student_container);
        }
    }


    public StudentListAdapter(Activity activity, List<Student> studentList) {
        this.activity = activity;
        this.studentList = studentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Student student = studentList.get(position);
        holder.student_name.setText(student.getFullName());
        holder.student_home.setText(student.getHomeCity());
        holder.student_host.setText(student.getHostCity());
        if (!student.isEmailPrivate() && student.getEmail() != null) {
            holder.student_contact.setVisibility(View.VISIBLE);

            holder.student_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{student.getEmail()});
                    activity.startActivity(Intent.createChooser(emailIntent, "Send email:"));
                }
            });
        }

        holder.student_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ChatActivity.class);
                intent.putExtra(EXTRA_CHAT_USER_ID, student.getStudentId());
                activity.startActivity(intent);
            }
        });

        Utils.setProfilePhoto(activity, holder.student_photo, student.getStudentId());
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }
}