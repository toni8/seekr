package com.antonio.seekr.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.antonio.seekr.models.Chat;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Antonio on 15/02/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    private final static int OTHER_CHAT = 0;
    private final static int MY_CHAT = 1;
    private final ArrayList<Chat> mChat;
    private final String myID;
    private Activity act;

    public ChatListAdapter(Activity act, String myID, ArrayList<Chat> chatList) {
        this.act = act;
        this.myID = myID;
        this.mChat = chatList;
    }

    public void add(Chat chat) {
        mChat.add(chat);
        notifyItemInserted(mChat.size() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        switch (viewType) {
            case OTHER_CHAT:
                layoutRes = R.layout.item_chat;
                break;
            case MY_CHAT:
                layoutRes = R.layout.item_chat_me;
                break;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
       if(mChat.get(position).getID().equalsIgnoreCase(myID)){
           return MY_CHAT;
       } else {
           return OTHER_CHAT;
       }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(holder.photo != null) {

            Utils.setProfilePhoto(act, holder.photo, mChat.get(position).getID());
        }

        holder.content.setText(mChat.get(position).getMessage());
        // Fill other views

        // If you wanted to check view type without checking if views exist
        int viewType = getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mChat.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView content;
        final ImageView photo;

        public ViewHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.tv_chat_msg);
            photo = (ImageView) itemView.findViewById(R.id.iv_chat_photo);
        }
    }
}