package com.antonio.seekr.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.seekr.models.Event;
import com.antonio.seekr.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;


public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> {

    private List<Event> eventList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, creator, time, location;
        public ImageView ivDelete;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.event_title);
            description = (TextView) view.findViewById(R.id.event_description);
            creator = (TextView) view.findViewById(R.id.event_creator);
            time = (TextView) view.findViewById(R.id.event_time);
            location = (TextView) view.findViewById(R.id.event_location);
            ivDelete = (ImageView) view.findViewById(R.id.event_delete);
        }
    }


    public EventListAdapter(Activity activity, List<Event> eventList) {
        this.activity = activity;
        this.eventList = eventList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Event event = eventList.get(position);
        String myID = "";

        FirebaseUser meUser = FirebaseAuth.getInstance().getCurrentUser();
        myID = meUser.getUid();

        //set title background color
        if (event.getColor() != null) {
            holder.title.setBackgroundColor(Color.parseColor(event.getColor()));
        }

        holder.title.setText(event.getTitle());
        holder.description.setText(event.getDescription());
        holder.creator.setText(event.getCreator());
        holder.time.setText(event.getTime());
        holder.location.setText(event.getCity() + " (" + event.getCountry() + ")");
        if (event.getCreatorId().equals(myID)) {
            holder.ivDelete.setVisibility(View.VISIBLE);
            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(activity)
                            .setTitle("Warning")
                            .setMessage("Are you sure you want to remove your event?")
                            .setIcon(R.drawable.ic_alert_warning)
                            .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("events");
                                    mDatabase.child(event.getId()).removeValue();
                                    Toast.makeText(activity, "Event removed.", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("Cancel", null).show();
                }
            });
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }
}