package com.antonio.seekr.adapters;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.antonio.seekr.activities.FirstLoginActivity;
import com.antonio.seekr.R;
import com.antonio.seekr.utils.Utils;
import com.antonio.seekr.utils.Validators;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.AddressComponentType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

/**
 * Created by Antonio on 04/11/2016.
 */

public class FirstLoginPagerAdapter extends PagerAdapter {
    //    private Context mContext;
    private Activity mActivity;
    private PlacesAutocompleteTextView homePlace, hostPlace;
    Place chosenPlace;
    private EditText fullNameEt, homeUniversityEt, hostUniversityEt;
    private TextInputLayout fullNameInput, homePlaceInput, homeUniversityInput, hostPlaceInput, hostUniversityInput;
    private String homeCity, homeCountry, hostCity, hostCountry;
    private Validators validators;

    public FirstLoginPagerAdapter(Activity activity) {
        mActivity = activity;
        validators = new Validators(mActivity);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);
        if (position == 0) {
            initPersonalPage(layout);
        } else if (position == 1) {
            initHomePage(layout);
        } else if (position == 2) {
            initAbroadPage(layout);
        }
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return CustomPagerEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        return mActivity.getString(customPagerEnum.getTitleResId());
    }

    private void initPersonalPage(ViewGroup layout) {
        fullNameEt = (EditText) layout.findViewById(R.id.et_fullname);
        fullNameInput = (TextInputLayout) layout.findViewById(R.id.input_layout_fullname);
        final ImageView profilePhotoIv = (ImageView) layout.findViewById(R.id.personal_info_photo);
        profilePhotoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mActivity instanceof FirstLoginActivity)
                    ((FirstLoginActivity) mActivity).photoChooser(profilePhotoIv);
            }
        });

    }

    private void initHomePage(ViewGroup layout) {

        homePlace = (PlacesAutocompleteTextView) layout.findViewById(R.id.et_home_place);
        homePlace.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {

                        homePlace.getDetailsFor(place, new DetailsCallback() {
                            @Override
                            public void onSuccess(final PlaceDetails details) {
                                for (AddressComponent component : details.address_components) {
                                    for (AddressComponentType type : component.types) {
                                        homeCity = details.vicinity;
                                        switch (type) {
                                            case STREET_NUMBER:
                                                break;
                                            case ROUTE:
                                                break;
                                            case NEIGHBORHOOD:
                                                break;
                                            case SUBLOCALITY_LEVEL_1:
                                                break;
                                            case SUBLOCALITY:
                                                break;
                                            case LOCALITY:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_1:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_2:
                                                break;
                                            case COUNTRY:
                                                homeCountry = Utils.clearFyrom(component.long_name);
                                                break;
                                            case POSTAL_CODE:
                                                break;
                                            case POLITICAL:
                                                break;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final Throwable failure) {
                                Log.d("test", "failure " + failure);
                            }
                        });

                    }
                }
        );


        homeUniversityEt = (EditText) layout.findViewById(R.id.et_homeuni);

        homePlaceInput = (TextInputLayout) layout.findViewById(R.id.input_layout_home_place);
        homeUniversityInput = (TextInputLayout) layout.findViewById(R.id.input_layout_homeuni);
    }

    private void initAbroadPage(ViewGroup layout) {

        hostPlace = (PlacesAutocompleteTextView) layout.findViewById(R.id.et_host_place);
        hostPlace.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {

                        hostPlace.getDetailsFor(place, new DetailsCallback() {
                            @Override
                            public void onSuccess(final PlaceDetails details) {
                                for (AddressComponent component : details.address_components) {
                                    for (AddressComponentType type : component.types) {
                                        hostCity = details.vicinity;
                                        switch (type) {
                                            case STREET_NUMBER:
                                                break;
                                            case ROUTE:
                                                break;
                                            case NEIGHBORHOOD:
                                                break;
                                            case SUBLOCALITY_LEVEL_1:
                                                break;
                                            case SUBLOCALITY:
                                                break;
                                            case LOCALITY:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_1:
                                                break;
                                            case ADMINISTRATIVE_AREA_LEVEL_2:
                                                break;
                                            case COUNTRY:
                                                hostCountry = Utils.clearFyrom(component.long_name);
                                                break;
                                            case POSTAL_CODE:
                                                break;
                                            case POLITICAL:
                                                break;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final Throwable failure) {
                                Log.d("test", "failure " + failure);
                            }
                        });

                    }
                }
        );

        hostPlaceInput = (TextInputLayout) layout.findViewById(R.id.input_layout_host_place);
        hostUniversityEt = (EditText) layout.findViewById(R.id.et_hostuni);
        hostUniversityInput = (TextInputLayout) layout.findViewById(R.id.input_layout_hostuni);

        ImageButton btn = (ImageButton) layout.findViewById(R.id.btn_registration_done);
        if (btn != null) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ///
                    //Validate fields
                    if (!validators.validateRequiredField(fullNameEt, fullNameInput)) {
                        if (mActivity instanceof FirstLoginActivity)
                            ((FirstLoginActivity) mActivity).goToPage(0);
                        return;
                    }

                    if (!validators.validateRequiredField(homePlace, homePlaceInput)
                            || !validators.validateRequiredField(homeUniversityEt, homeUniversityInput)) {
                        if (mActivity instanceof FirstLoginActivity)
                            ((FirstLoginActivity) mActivity).goToPage(1);
                        return;
                    }

                    if (!validators.validateRequiredField(hostPlace, hostPlaceInput)
                            || !validators.validateRequiredField(hostUniversityEt, hostUniversityInput)) {
                        return;
                    }
                    ///
                    //

                    if (mActivity instanceof FirstLoginActivity)
                        ((FirstLoginActivity) mActivity).updateUserInfo(fullNameEt.getText().toString(),
                                homeCity, hostCity, homeCountry, hostCountry, homeUniversityEt.getText().toString(), hostUniversityEt.getText().toString());


                }
            });
        }
    }

    public enum CustomPagerEnum {

        PERSONAL(R.string.page_personal_info, R.layout.page_personal_info),
        HOME(R.string.page_home_info, R.layout.page_home_info),
        HOST(R.string.page_host_info, R.layout.page_host_info);

        private int mTitleResId;
        private int mLayoutResId;

        CustomPagerEnum(int titleResId, int layoutResId) {
            mTitleResId = titleResId;
            mLayoutResId = layoutResId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }

        public int getLayoutResId() {
            return mLayoutResId;
        }

    }
}
