package com.antonio.seekr.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.antonio.seekr.fragments.EventsFragment;
import com.antonio.seekr.fragments.MenuFragment;
import com.antonio.seekr.fragments.StudentsFragment;

/**
 * Created by Antonio on 31-Oct-16.
 */

public class FragmentsAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public FragmentsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return EventsFragment.newInstance(0);
            case 1:
                return StudentsFragment.newInstance(1);
            case 2:
                return MenuFragment.newInstance(2);
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
