package com.antonio.seekr.models;

/**
 * Created by Antonio on 25-Oct-16.
 */

public class Student {

    private String studentId, fullName, homeUni, hostUni, homeCity, hostCity, homeCountry, hostCountry, email;
    private int firstLogin;
    private boolean emailPrivate;

    public Student(String fullName, String homeUni, String hostUni, String homeCity, String hostCity, String homeCountry, String hostCountry) {
        this.fullName = fullName;
        this.homeUni = homeUni;
        this.hostUni = hostUni;
        this.homeCity = homeCity;
        this.hostCity = hostCity;
        this.homeCountry = homeCountry;
        this.hostCountry = hostCountry;
    }

    public Student() {
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(boolean emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHomeUni() {
        return homeUni;
    }

    public void setHomeUni(String homeUni) {
        this.homeUni = homeUni;
    }

    public String getHostUni() {
        return hostUni;
    }

    public void setHostUni(String hostUni) {
        this.hostUni = hostUni;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getHostCity() {
        return hostCity;
    }

    public void setHostCity(String hostCity) {
        this.hostCity = hostCity;
    }

    public String getHomeCountry() {
        return homeCountry;
    }

    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    public String getHostCountry() {
        return hostCountry;
    }

    public void setHostCountry(String hostCountry) {
        this.hostCountry = hostCountry;
    }

    public int getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(int firstLogin) {
        this.firstLogin = firstLogin;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fullName='" + fullName + '\'' +
                ", homeCity='" + homeCity + '\'' +
                ", hostCity='" + hostCity + '\'' +
                '}';
    }
}
