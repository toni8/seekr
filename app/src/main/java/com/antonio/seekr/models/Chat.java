package com.antonio.seekr.models;

/**
 * Created by Antonio on 15/02/2017.
 */

public class Chat {
    private String message;
    private String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
