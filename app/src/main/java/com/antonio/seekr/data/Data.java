package com.antonio.seekr.data;

import com.antonio.seekr.models.Event;
import com.antonio.seekr.models.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 23/11/2016.
 */

public class Data {
    List<Event> eventList = new ArrayList<>();
    List<Student> studentList = new ArrayList<>();
    Student me = new Student();

    private static Data instance = null;

    private Data() {
    }

    public static Data getInstance() {
        if (instance == null) {
            instance = new Data();
        }
        return instance;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public Student getMe() {
        return me;
    }

    public void setMe(Student me) {
        this.me = me;
    }
}
